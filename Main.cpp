#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <sstream>
#include <queue>
#include <fstream>
#include "pstream.h"


bool checkPassword(const std::string &password, const std::string &filename);

const std::string searchPassword(const std::string &startpass, const std::string &filename, int maxlen);

const std::string searchPassFromDict(const std::string &filename);

char symbols[] = "0123456789ETAOINSHRDLCUMWFGYPBVKXJQZetaoinshrdlcumwfgypbvkxjqz.";

long long int t;

int main() {
	std::string s = "", filename;
	while (access(filename.c_str(), 0) == -1) {
		std::cout << "Enter filename: ";
		std::cin >> filename;
	}
	std::cout << "Searching password for " << filename << "\n";
	t = time(NULL);
	try {
		std::cout << (time(NULL) - t) << ": " << "Searching from dict\n";
		std::cout << (time(NULL) - t) << ": " << "Pass found: " << searchPassFromDict(filename) << "\n";
	} catch (const char *str) {
		try {
			std::cout << (time(NULL) - t) << ": " << "Searching from all possible variants\n";
			std::cout << (time(NULL) - t) << ": " << "Pass found: " << searchPassword(s, filename, 20) << "\n";
		} catch (const char *str) {
			std::cout << (time(NULL) - t) << ": " << "Error: " << str << "\n";
		}
	}
	std::cout << "Finish time: " << (time(NULL) - t);
	return 0;
}

std::queue<std::string> passQueue;
std::string buf;

const std::string searchPassFromDict(const std::string &filename) {
	std::string pass = "";
	std::ifstream dict("dict");
	while (dict >> pass) {
		if (checkPassword(pass, filename)) {
			dict.close();
			return pass;
		}
	}
	dict.close();
	throw "Pass not found";
}

unsigned long curBufTry = 0;
unsigned long curQ = 1;
int curLen = 1;

const std::string searchPassword(const std::string &startpass, const std::string &filename, int maxlen) {
	const size_t numOfSyms = sizeof(symbols) - 1;
	passQueue.push(startpass);
	while (!passQueue.empty()) {
		buf = passQueue.front();
		passQueue.pop();
		curBufTry++;
		if ((curBufTry % curQ == 0)) {
			std::cout << (time(NULL) - t) << ": " << "Current pass len: " << curLen++ << "\n";
			curQ *= numOfSyms;
		}
		for (int i = 0; i < numOfSyms; ++i) {
			if (checkPassword(buf + symbols[i], filename))
				return buf + symbols[i];
			else
				passQueue.push(buf + symbols[i]);
		}
	}
	throw "Pass not found";
}


bool checkPassword(const std::string &password, const std::string &filename) {
	std::stringstream ss;
	ss << "unzip -t -P " << password << " " << filename;
	redi::ipstream proc(ss.str(), redi::pstreams::pstdout | redi::pstreams::pstderr);
	std::string line;
	std::getline(proc.out(), line);
	std::getline(proc.out(), line);
	return line[line.size() - 1] == 'K';
}